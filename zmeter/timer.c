/* Copyright(C) 2008 Jochen Roessner <jochen@lugrot.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#include <stdint.h>

#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include "../config.h"
#include "../stella/stella.h"

//#define SWERTMAX 0xCB
#define SWERTMAX 0x80
#define DISPLAYMAX 0xDE

static uint8_t swertmax = SWERTMAX;
static uint8_t displaymax = DISPLAYMAX;

void
zmeter_timer (void)
{
    if((DDRD & _BV(PD5)) == 0)
    {
      uint16_t adc0,adc1,tmp;
      ADMUX = (ADMUX & 0xF0);
      /* Start adc conversion */
      ADCSRA |= _BV(ADSC);
      /* Wait for completion of adc */
      while (ADCSRA & _BV(ADSC)) {}
      adc0 = ADC;
      ADMUX = (ADMUX & 0xF0) | 1;
      /* Start adc conversion */
      ADCSRA |= _BV(ADSC);
      /* Wait for completion of adc */
      while (ADCSRA & _BV(ADSC)) {}
      adc1 = ADC;
      
      if((PORTD & _BV(PD5)) == 0)
      {
        if(adc1 > swertmax)
          swertmax = adc1;
        tmp = ((adc1 * displaymax) / swertmax) & 0xFF ;
      }
      else
      {
        tmp = (adc0 >> 2);
        displaymax = tmp;
      }
      stella_fade[0] = tmp;
      stella_color[0] = tmp;
      stella_sort (stella_color);
      tmp = adc1;
      if(tmp > 20)
        tmp = (tmp * tmp) / (swertmax - (0xff - swertmax));
        //tmp = (tmp << 8) / (swertmax - (0xff - swertmax));
      else
        tmp = 0;
      if(tmp > 0xff)
        tmp = 0xff;
      stella_fade[3] = tmp;
      //stella_color[3] = tmp;
      if(tmp > 230)
        tmp = 0xff;
      stella_fade[1] = 0xff - tmp;
      //stella_color[1] = 0xff - tmp;
      if ((adc0 & 0x300) == 0x0)
        stella_fade[2] = adc0 & 0xff;
      //stella_color[2] = (adc0 >> 2) & 0xff;
      stella_sort (stella_color);
    }
    else
      swertmax = SWERTMAX;
}
